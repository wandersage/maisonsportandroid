package com.easternpeak.maisonsport.mvp.views;

import com.easternpeak.maisonsport.api.models.Notification;

import java.util.List;

/**
 * Created by antonandreev on 12.03.18.
 */

public interface INotificationsView extends BaseProgressView{
    void onLoaded(List<Notification> notifications);
}
