package com.easternpeak.maisonsport.mvp.presenters;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.blog.IBlogApiClient;
import com.easternpeak.maisonsport.api.models.Blog;
import com.easternpeak.maisonsport.mvp.views.IBlogView;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by antonandreev on 26.02.18.
 */
@InjectViewState
public class BlogPresenter extends BasePresenter<IBlogView> {

    @Inject
    IBlogApiClient blogApiClient;
    @Inject
    Context context;
    private final long blogId;

    public BlogPresenter(long blogId) {
        this.blogId = blogId;
        MaisonSportApplication.getComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getBlog(blogId);
    }

    public void getBlog(long blogId) {
        getViewState().showProgress();
        unsubscribeOnDestroy(blogApiClient.getBlog(blogId).subscribeWith(new DisposableObserver<Blog>() {
            @Override
            public void onNext(Blog blog) {
                getViewState().hideProgress();
                getViewState().onLoaded(blog);
            }

            @Override
            public void onError(Throwable e) {
                getViewState().hideProgress();
                getViewState().onError(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        }));
    }

    public void addComment(String message) {
        getViewState().showProgress();
        unsubscribeOnDestroy(blogApiClient.sendComment(message,blogId).subscribeWith(new DisposableObserver<Boolean>() {
            @Override
            public void onNext(Boolean aBoolean) {
//                getViewState().hideProgress();
                if (!aBoolean) {
                    getViewState().hideProgress();
                    getViewState().onError(context.getString(R.string.message_send_error));
                } else{
                    getViewState().onCommentSent();
                    getBlog(blogId);
                }
            }

            @Override
            public void onError(Throwable e) {
                getViewState().onError(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        }));
    }
}
