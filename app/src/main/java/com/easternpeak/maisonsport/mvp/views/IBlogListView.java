package com.easternpeak.maisonsport.mvp.views;

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.easternpeak.maisonsport.api.models.Blog;

import java.util.List;

/**
 * Created by antonandreev on 26.02.18.
 */

public interface IBlogListView extends BaseProgressView {
    void onLoaded(List<Blog> blogs);
}
