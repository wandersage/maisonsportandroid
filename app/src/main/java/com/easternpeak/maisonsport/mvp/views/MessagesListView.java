package com.easternpeak.maisonsport.mvp.views;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.easternpeak.maisonsport.api.models.Thread;

import java.util.List;

/**
 * Created by antonandreev on 22.02.18.
 */

public interface MessagesListView extends BaseProgressView {

    void onMessageLoaded(List<Thread> messageThreads);

}
