package com.easternpeak.maisonsport.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Patterns;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.arellomobile.mvp.InjectViewState;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.login.ILoginApiClient;
import com.easternpeak.maisonsport.fcm.MyFirebaseInstanceIDService;
import com.easternpeak.maisonsport.mvp.views.LoginView;
import com.easternpeak.maisonsport.ui.activities.MainActivity;
import com.easternpeak.maisonsport.ui.fragments.LoginFragment;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by antonandreev on 22.02.18.
 */
@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {
    @Inject
    ILoginApiClient loginApiClient;
    @Inject
    Context context;
    @Inject
    PreferenceUtils preferenceUtils;

    public LoginPresenter() {
        MaisonSportApplication.getComponent().inject(this);
    }

    public void signIn(String email, String password) {
        getViewState().hideErrors();
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches())
            getViewState().showEmailError();
        else if (password.isEmpty() || password.length() < 6)
            getViewState().showPasswordError();
        else {
            getViewState().showProgress();
            Disposable disposable = loginApiClient.signIn(email, password)
                    .subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            getViewState().hideProgress();
                            getViewState().hideErrors();
                            if (aBoolean)
                                getViewState().onLoginSuccess();
                            else
                                getViewState().onError(context.getString(R.string.login_error));
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            getViewState().hideProgress();
                            getViewState().hideErrors();
                            if (throwable instanceof HttpException)
                                getViewState().onError(context.getString(R.string.login_error));
                            else getViewState().onError(throwable.getMessage());
                        }
                    });
            unsubscribeOnDestroy(disposable);
        }

    }

    public void registerPush() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(LoginPresenter.class.getSimpleName(), "push token " + token);
        if (token != null) {
            getViewState().showProgress();
            unsubscribeOnDestroy(loginApiClient.registerPush(token)
                    .subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            getViewState().hideProgress();
                            if (aBoolean)
                                getViewState().onPushRegistered();
                            else
                                getViewState().onError(context.getString(R.string.login_error_push));
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            getViewState().hideProgress();
                            getViewState().onError(throwable.getMessage());
                        }
                    }));
        } else
            getViewState().onPushRegistered();

    }

    public void unregisterPush() {
        Log.d(MyFirebaseInstanceIDService.TAG, "unregister push");
        unsubscribeOnDestroy(loginApiClient.unregisterPush()
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        getViewState().hideProgress();
                        getViewState().onPushUnRegistered();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().hideProgress();
                        getViewState().onError(throwable.getMessage());
                    }
                }));
    }

    public void signOut() {
        getViewState().showProgress();
        Disposable disposable = loginApiClient.signOut()
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> responseBodyResponse) throws Exception {
                        getViewState().hideProgress();
                        if (responseBodyResponse.isSuccessful()) {
                            CookieSyncManager.createInstance(context);
                            CookieManager cookieManager = CookieManager.getInstance();
                            cookieManager.removeAllCookie();
                            getViewState().onLogoutSuccess();
                        } else if (!responseBodyResponse.isSuccessful() && responseBodyResponse.code() == 401) {
                            preferenceUtils.clear();
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        } else
                            throw new Exception(responseBodyResponse.message());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().hideProgress();
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).code() == 401) {
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                            }
                        } else
                            getViewState().onError(throwable.getMessage());
                    }
                });
        unsubscribeOnDestroy(disposable);
    }
}
