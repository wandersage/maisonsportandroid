package com.easternpeak.maisonsport.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by antonandreev on 22.02.18.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface LoginView extends BaseProgressView {
    void showEmailError();

    void showPasswordError();

    void hideErrors();

    @StateStrategyType(SkipStrategy.class)
    void onLoginSuccess();

    @StateStrategyType(SkipStrategy.class)
    void onPushRegistered();

    void onLogoutSuccess();

    void onPushUnRegistered();
}
