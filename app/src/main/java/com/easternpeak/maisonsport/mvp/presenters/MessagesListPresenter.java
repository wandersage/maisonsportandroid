package com.easternpeak.maisonsport.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.api.messages.IMessagesApiClient;
import com.easternpeak.maisonsport.api.models.Thread;
import com.easternpeak.maisonsport.mvp.views.MessagesListView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by antonandreev on 22.02.18.
 */
@InjectViewState
public class MessagesListPresenter extends BasePresenter<MessagesListView> {
    @Inject
    IMessagesApiClient messagesApiClient;

    public MessagesListPresenter() {
        MaisonSportApplication.getComponent().inject(this);
    }

    public void getMessages() {
        getViewState().showProgress();
        unsubscribeOnDestroy(messagesApiClient.getThreads().subscribeWith(new DisposableObserver<List<Thread>>() {
            @Override
            public void onNext(List<Thread> messageThreads) {
                getViewState().onMessageLoaded(messageThreads);
                getViewState().hideProgress();
            }

            @Override
            public void onError(Throwable e) {
                getViewState().hideProgress();
                getViewState().onError(e.getMessage());
            }

            @Override
            public void onComplete() {
//                getViewState().hideProgress();
            }
        }));
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getMessages();
    }
}
