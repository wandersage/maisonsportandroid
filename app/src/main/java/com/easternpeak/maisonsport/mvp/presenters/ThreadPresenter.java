package com.easternpeak.maisonsport.mvp.presenters;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.messages.IMessagesApiClient;
import com.easternpeak.maisonsport.api.models.MessageThread;
import com.easternpeak.maisonsport.mvp.views.ThreadView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by antonandreev on 23.02.18.
 */
@InjectViewState
public class ThreadPresenter extends BasePresenter<ThreadView> {
    @Inject
    IMessagesApiClient apiClient;
    @Inject
    Context context;

    public ThreadPresenter() {
        MaisonSportApplication.getComponent().inject(this);
    }

    public void getThread(final long threadId) {
        if (threadId > 0) {
            getViewState().showProgress();
            Disposable disposable = apiClient.getThread(threadId)
                    .subscribe(new Consumer<MessageThread>() {
                        @Override
                        public void accept(MessageThread messageThread) throws Exception {
                            getViewState().hideProgress();
                            getViewState().onLoaded(messageThread);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            getViewState().hideProgress();
                            getViewState().onError(throwable.getMessage());
                        }
                    });
            unsubscribeOnDestroy(disposable);

        }
    }

    public void sendMessage(long  threadId, final String message) {
        if (message.isEmpty() || message.trim().isEmpty())
            getViewState().onError(context.getString(R.string.message_error));
        else {
            getViewState().showProgress();
            Disposable disposable = apiClient.sendMessage(threadId,message)
                    .subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            getViewState().hideProgress();
                            if (aBoolean) {
                                getViewState().onMessageSent();
                            } else
                                getViewState().onError(context.getString(R.string.message_send_error));
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            getViewState().hideProgress();
                            getViewState().onError(throwable.getMessage());
                        }
                    });
            unsubscribeOnDestroy(disposable);
        }
    }

}
