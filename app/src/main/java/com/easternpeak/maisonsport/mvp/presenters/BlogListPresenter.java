package com.easternpeak.maisonsport.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.api.blog.IBlogApiClient;
import com.easternpeak.maisonsport.api.models.Blog;
import com.easternpeak.maisonsport.mvp.views.IBlogListView;
import com.easternpeak.maisonsport.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;

/**
 * Created by antonandreev on 26.02.18.
 */

@InjectViewState
public class BlogListPresenter extends BasePresenter<IBlogListView> {
    @Inject
    IBlogApiClient blogApiClient;
    @Inject
    PreferenceUtils preferenceUtils;

    public BlogListPresenter() {
        MaisonSportApplication.getComponent().inject(this);
    }

    public void getBlogs() {
        Observable<List<Blog>> listObservable = blogApiClient.getBlogs(preferenceUtils.isInstructor() ? 2 : 1);
        Disposable disposable = listObservable.subscribe(new Consumer<List<Blog>>() {
            @Override
            public void accept(List<Blog> blogs) throws Exception {
                getViewState().hideProgress();
                getViewState().onLoaded(blogs);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                getViewState().hideProgress();
                getViewState().onError(throwable.getMessage());
            }
        });
        unsubscribeOnDestroy(disposable);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getBlogs();
    }
}
