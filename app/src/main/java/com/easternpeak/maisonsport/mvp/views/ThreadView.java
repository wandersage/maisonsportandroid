package com.easternpeak.maisonsport.mvp.views;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.easternpeak.maisonsport.api.models.MessageThread;

/**
 * Created by antonandreev on 23.02.18.
 */

public interface ThreadView extends BaseProgressView {
    void onLoaded(MessageThread messageThread);

    @StateStrategyType(SkipStrategy.class)
    void onMessageSent();
}
