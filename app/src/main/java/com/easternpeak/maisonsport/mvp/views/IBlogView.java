package com.easternpeak.maisonsport.mvp.views;

import com.easternpeak.maisonsport.api.models.Blog;

/**
 * Created by antonandreev on 26.02.18.
 */

public interface IBlogView extends BaseProgressView{
    void onLoaded(Blog blog);

    void onCommentSent();

}
