package com.easternpeak.maisonsport.mvp.views;

/**
 * Created by antonandreev on 23.02.18.
 */

public interface IForgotPasswordView extends BaseProgressView {

    void onEmailError();

    void onSuccess();
}
