package com.easternpeak.maisonsport.mvp.presenters;

import android.content.Context;
import android.util.Patterns;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.login.ILoginApiClient;
import com.easternpeak.maisonsport.mvp.views.IForgotPasswordView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by antonandreev on 23.02.18.
 */

@InjectViewState
public class ForgotPasswordPresenter extends BasePresenter<IForgotPasswordView> {
    @Inject
    ILoginApiClient apiClient;
    @Inject
    Context context;

    public ForgotPasswordPresenter() {
        MaisonSportApplication.getComponent().inject(this);
    }

    public void forgotPassword(String email) {
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches())
            getViewState().onEmailError();
        else {
            getViewState().showProgress();
            Disposable disposable = apiClient.forgotPassword(email)
                    .subscribe(new Consumer<Response<ResponseBody>>() {
                        @Override
                        public void accept(Response<ResponseBody> responseBodyResponse) throws Exception {
                            getViewState().hideProgress();
                            if (responseBodyResponse.isSuccessful())
                                getViewState().onSuccess();
                            else getViewState().onError(context.getString(R.string.login_error));
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            getViewState().hideProgress();
                            getViewState().onError(throwable.getMessage());
                        }
                    });
            unsubscribeOnDestroy(disposable);

        }
    }
}
