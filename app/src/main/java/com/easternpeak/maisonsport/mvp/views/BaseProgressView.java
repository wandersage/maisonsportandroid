package com.easternpeak.maisonsport.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by antonandreev on 22.02.18.
 */

public interface BaseProgressView extends MvpView {
    void showProgress();

    void hideProgress();

    @StateStrategyType(SkipStrategy.class)
    void onError(String message);
}
