package com.easternpeak.maisonsport.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.api.models.Notification;
import com.easternpeak.maisonsport.api.notifications.INotificationsApiClient;
import com.easternpeak.maisonsport.mvp.views.INotificationsView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by antonandreev on 12.03.18.
 */

@InjectViewState
public class NotificationsPresenter extends BasePresenter<INotificationsView> {

    @Inject
    INotificationsApiClient apiClient;

    public NotificationsPresenter() {
        MaisonSportApplication.getComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getNotifications();
    }

    public void getNotifications() {
        getViewState().showProgress();
        unsubscribeOnDestroy(apiClient.getNotifications()
                .subscribe(new Consumer<List<Notification>>() {
                    @Override
                    public void accept(List<Notification> notifications) throws Exception {
                        getViewState().hideProgress();
                        getViewState().onLoaded(notifications);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().hideProgress();
                        getViewState().onError(throwable.getMessage());
                    }
                }));
    }

    public void removeNotificaiton(long id) {
        unsubscribeOnDestroy(apiClient.removeNotifications(id)
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> responseBodyResponse) throws Exception {

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                    }
                }));
    }
}
