package com.easternpeak.maisonsport.api.messages;

import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.api.models.MessageThread;
import com.easternpeak.maisonsport.api.models.Thread;
import com.easternpeak.maisonsport.utils.PreferenceUtils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by antonandreev on 22.02.18.
 */

public class IMessagesApiClientImpl implements IMessagesApiClient {
    @Inject
    PreferenceUtils preferenceUtils;
    private final IMessagesApi api;

    public IMessagesApiClientImpl(Retrofit retrofit) {
        this.api = retrofit.create(IMessagesApi.class);
        MaisonSportApplication.getComponent().inject(this);
    }

    @Override
    public Observable<List<Thread>> getThreads() {
        return api.getThreads()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<MessageThread> getThread(long threadId) {
        return api.getThread(threadId)
                .flatMap(new Function<MessageThread, ObservableSource<MessageThread>>() {
                    @Override
                    public ObservableSource<MessageThread> apply(MessageThread messageThread) throws Exception {
                        Collections.reverse(messageThread.getMessages());
                        return Observable.just(messageThread);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> sendMessage(long threadId, String message) {
        return api.addMessage(threadId, message)
                .map(new Function<Response<ResponseBody>, Boolean>() {
                    @Override
                    public Boolean apply(Response<ResponseBody> responseBodyResponse) throws Exception {
                        return responseBodyResponse.isSuccessful();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
