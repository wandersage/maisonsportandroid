package com.easternpeak.maisonsport.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonandreev on 26.02.18.
 */

public class Blog implements Parcelable {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("user_id")
    @Expose
    private long userId;
    @SerializedName("category_id")
    @Expose
    private long categoryId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("status_id")
    @Expose
    private long statusId;
    @SerializedName("visibility_id")
    @Expose
    private long visibilityId;
    @SerializedName("reviewer_id")
    @Expose
    private long reviewerId;
    @SerializedName("publish_date")
    @Expose
    private String publishDate;
    @SerializedName("being_edited_by")
    @Expose
    private String beingEditedBy;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("locale")
    @Expose
    private String locale;
    @SerializedName("date_and_min_read")
    @Expose
    private String dateAndMinRead;
    @SerializedName("comment")
    @Expose
    private List<Comment> comment = null;

    public long getId() {
        return id;
    }

    public String getHash() {
        return hash;
    }

    public String getTitle() {
        return title;
    }

    public String getSlug() {
        return slug;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getContent() {
        return content;
    }

    public long getUserId() {
        return userId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public long getStatusId() {
        return statusId;
    }

    public long getVisibilityId() {
        return visibilityId;
    }

    public long getReviewerId() {
        return reviewerId;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public String getBeingEditedBy() {
        return beingEditedBy;
    }

    public String getPassword() {
        return password;
    }

    public String getLocale() {
        return locale;
    }

    public String getDateAndMinRead() {
        return dateAndMinRead;
    }

    public List<Comment> getComment() {
        return comment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.hash);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeString(this.shortDescription);
        dest.writeString(this.content);
        dest.writeLong(this.userId);
        dest.writeLong(this.categoryId);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.deletedAt);
        dest.writeLong(this.statusId);
        dest.writeLong(this.visibilityId);
        dest.writeLong(this.reviewerId);
        dest.writeString(this.publishDate);
        dest.writeString(this.beingEditedBy);
        dest.writeString(this.password);
        dest.writeString(this.locale);
        dest.writeString(this.dateAndMinRead);
        dest.writeList(this.comment);
    }

    public Blog() {
    }

    protected Blog(Parcel in) {
        this.id = in.readLong();
        this.hash = in.readString();
        this.title = in.readString();
        this.slug = in.readString();
        this.shortDescription = in.readString();
        this.content = in.readString();
        this.userId = in.readLong();
        this.categoryId = in.readLong();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.deletedAt = in.readString();
        this.statusId = in.readLong();
        this.visibilityId = in.readLong();
        this.reviewerId = in.readLong();
        this.publishDate = in.readString();
        this.beingEditedBy = in.readString();
        this.password = in.readString();
        this.locale = in.readString();
        this.dateAndMinRead = in.readString();
        this.comment = new ArrayList<Comment>();
        in.readList(this.comment, Comment.class.getClassLoader());
    }

    public static final Creator<Blog> CREATOR = new Creator<Blog>() {
        @Override
        public Blog createFromParcel(Parcel source) {
            return new Blog(source);
        }

        @Override
        public Blog[] newArray(int size) {
            return new Blog[size];
        }
    };
}
