package com.easternpeak.maisonsport.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by antonandreev on 12.03.18.
 */

public class Notification {

    @SerializedName("post_id")
    @Expose
    private long postId = -1;
//    @SerializedName("category_id")
//    @Expose
//    private long categoryId;

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("thread_id")
    @Expose
    private long threadId = -1;
    @SerializedName("for_user")
    @Expose
    private long forUser;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("avatar")
    @Expose
    private String avatar;

    public long getPostId() {
        return postId;
    }

//    public long getCategoryId() {
//        return categoryId;
//    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public long getThreadId() {
        return threadId;
    }

    public long getForUser() {
        return forUser;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getAvatar() {
        return avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        if (postId != that.postId) return false;
        if (id != that.id) return false;
        if (threadId != that.threadId) return false;
        if (forUser != that.forUser) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null)
            return false;
        return avatar != null ? avatar.equals(that.avatar) : that.avatar == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (postId ^ (postId >>> 32));
        result = 31 * result + (int) (id ^ (id >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (int) (threadId ^ (threadId >>> 32));
        result = 31 * result + (int) (forUser ^ (forUser >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        return result;
    }
}
