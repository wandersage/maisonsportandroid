package com.easternpeak.maisonsport.api.messages;

import com.easternpeak.maisonsport.api.models.AddMessageResponse;
import com.easternpeak.maisonsport.api.models.Thread;
import com.easternpeak.maisonsport.api.models.MessageThread;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by antonandreev on 22.02.18.
 */

interface IMessagesApi {
    @GET("user/all-threads")
    Observable<List<Thread>> getThreads();

    @GET("user/{threadId}/all-messages")
    Observable<MessageThread> getThread(@Path("threadId") long threadId);

    @POST("user/{threadId}/add-message")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> addMessage(@Path("threadId") long threadId, @Field("message") String message);
}
