
package com.easternpeak.maisonsport.api.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thread {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("created_at")
    @Expose
    private DateAt createdAt;
//    @SerializedName("updated_at")
//    @Expose
//    private DateAt updatedAt;
    @SerializedName("active_at")
    @Expose
    private DateAt activeAt;
//    @SerializedName("lastMessageInTread")
//    @Expose
//    private LastMessageInThread lastMessagesInTread;
    @SerializedName("bookingResort")
    @Expose
    private BookingResort bookingResort;
    @SerializedName("bookingStart")
    @Expose
    private DateAt bookingStart;
//    @SerializedName("sumUnreadMessages")
//    @Expose
//    private long sumUnreadMessages;
    @SerializedName("respondent_chat")
    @Expose
    private RespondentChat respondentChat;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DateAt getCreatedAt() {
        return createdAt;
    }


    public DateAt getActiveAt() {
        return activeAt;
    }


    public BookingResort getBookingResort() {
        return bookingResort;
    }

    public DateAt getBookingStart() {
        return bookingStart;
    }

    public RespondentChat getRespondentChat() {
        return respondentChat;
    }

}
