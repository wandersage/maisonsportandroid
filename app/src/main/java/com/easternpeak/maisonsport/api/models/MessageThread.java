
package com.easternpeak.maisonsport.api.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageThread {

    @SerializedName("thread_id")
    @Expose
    private long threadId;
    @SerializedName("respondent_chat")
    @Expose
    private RespondentChat respondentChat;
    @SerializedName("current_user")
    @Expose
    private CurrentUser currentUser;
    @SerializedName("sidebar_data")
    @Expose
    private SidebarData sidebarData;
    @SerializedName("messages")
    @Expose
    private List<Message> messages = null;

    public long getThreadId() {
        return threadId;
    }

    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    public RespondentChat getRespondentChat() {
        return respondentChat;
    }

    public void setRespondentChat(RespondentChat respondentChat) {
        this.respondentChat = respondentChat;
    }

    public CurrentUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }

    public SidebarData getSidebarData() {
        return sidebarData;
    }

    public void setSidebarData(SidebarData sidebarData) {
        this.sidebarData = sidebarData;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

}
