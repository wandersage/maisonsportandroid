package com.easternpeak.maisonsport.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by antonandreev on 22.02.18.
 */

public class DateAt {
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("timezone_type")
    @Expose
    private long timezoneType;
    @SerializedName("timezone")
    @Expose
    private String timezone;

    public String getDate() {
        return date;
    }

    public long getTimezoneType() {
        return timezoneType;
    }

    public String getTimezone() {
        return timezone;
    }
}
