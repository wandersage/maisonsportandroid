package com.easternpeak.maisonsport.api.messages;

import com.easternpeak.maisonsport.api.models.MessageThread;
import com.easternpeak.maisonsport.api.models.Thread;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by antonandreev on 22.02.18.
 */

public interface IMessagesApiClient {
    Observable<List<Thread>> getThreads();

    Observable<MessageThread> getThread(long threadId);

    Observable<Boolean> sendMessage(long threadId, String message);
}
