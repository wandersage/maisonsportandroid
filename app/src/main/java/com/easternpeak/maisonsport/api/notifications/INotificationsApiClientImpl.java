package com.easternpeak.maisonsport.api.notifications;

import com.easternpeak.maisonsport.api.models.Notification;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by antonandreev on 12.03.18.
 */

public class INotificationsApiClientImpl implements INotificationsApiClient {
    private final INotificationsApi api;

    public INotificationsApiClientImpl(Retrofit retrofit) {
        this.api = retrofit.create(INotificationsApi.class);
    }

    @Override
    public Observable<List<Notification>> getNotifications() {
        return api.getNotifications()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Response<ResponseBody>> removeNotifications(long id) {
        return api.removeNotifications(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
