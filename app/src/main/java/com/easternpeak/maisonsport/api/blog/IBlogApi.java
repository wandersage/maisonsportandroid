package com.easternpeak.maisonsport.api.blog;

import com.easternpeak.maisonsport.api.models.Blog;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by antonandreev on 26.02.18.
 */

interface IBlogApi {
    @GET("blog/all-posts/{type}")
    Observable<List<Blog>> getBlogs(@Path("type") int type);

    @GET("blog/post/{id}")
    Observable<Blog> getBlog(@Path("id") long id);

    @POST("blog/post/{id}/add-comment")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> sendComment(@Field("comment") String message,@Path("id")long blogId);
}
