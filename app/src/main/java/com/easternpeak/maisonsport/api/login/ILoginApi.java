package com.easternpeak.maisonsport.api.login;

import com.easternpeak.maisonsport.api.models.SignInResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by antonandreev on 21.02.18.
 */

interface ILoginApi {

    @POST("/api/user/login")
    @FormUrlEncoded
    Observable<Response<SignInResponse>> signIn(@Field("email") String email, @Field("password") String password);
    @POST("/api/user/forgot-password")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> forgotPassword(@Field("email") String email);

    @POST("/api/user/logout")
//    @FormUrlEncoded
    Observable<Response<ResponseBody>> signOut();

    @POST("/api/user/device-token")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> sendPushToken(@Field("device_token") String refreshedToken);
}
