package com.easternpeak.maisonsport.api.login;

import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.easternpeak.maisonsport.BuildConfig;
import com.easternpeak.maisonsport.api.models.SignInResponse;
import com.easternpeak.maisonsport.di.modules.RetrofitModule;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by antonandreev on 21.02.18.
 */

public class ILoginApiClientImpl implements ILoginApiClient {
    private final ILoginApi loginApi;
    private final PreferenceUtils preferenceUtils;

    public ILoginApiClientImpl(Retrofit retrofit, PreferenceUtils preferenceUtils) {
        loginApi = retrofit.create(ILoginApi.class);
        this.preferenceUtils = preferenceUtils;
    }

    @Override
    public Observable<Boolean> signIn(String email, String password) {
        return loginApi.signIn(email, password)
                .doOnNext(new Consumer<Response<SignInResponse>>() {
                    @Override
                    public void accept(Response<SignInResponse> signInResponse) throws Exception {
                        SignInResponse response = signInResponse.body();
                        if (signInResponse.isSuccessful() && response != null) {
                            preferenceUtils.setToken(response.getAccessToken());
                            preferenceUtils.setLanguage(response.getLang());
                            preferenceUtils.setTokenType(response.getTokenType());
                            preferenceUtils.setType(response.getType());
                        }
                    }
                }).flatMap(new Function<Response<SignInResponse>, ObservableSource<Boolean>>() {
                    @Override
                    public ObservableSource<Boolean> apply(Response<SignInResponse> signInResponseResponse) throws Exception {
                        return Observable.just(signInResponseResponse.isSuccessful());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Response<ResponseBody>> signOut() {
        return loginApi.signOut()
                .doOnNext(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> responseBody) throws Exception {
                        if (responseBody.isSuccessful()) {
                            try {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            preferenceUtils.clear();
                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Response<ResponseBody>> forgotPassword(String email) {
        return loginApi.forgotPassword(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> registerPush(final String refreshedToken) {
        return loginApi.sendPushToken(refreshedToken)
                .map(new Function<Response<ResponseBody>, Boolean>() {
                    @Override
                    public Boolean apply(Response<ResponseBody> responseBodyResponse) throws Exception {
                        if (responseBodyResponse.isSuccessful())
                            preferenceUtils.setPushToken(refreshedToken);
                        return responseBodyResponse.isSuccessful();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> unregisterPush() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                FirebaseInstanceId.getInstance().deleteInstanceId();
                preferenceUtils.setPushToken(null);
                return true;
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
