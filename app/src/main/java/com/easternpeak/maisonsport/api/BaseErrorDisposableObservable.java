package com.easternpeak.maisonsport.api;

import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by antonandreev on 22.02.18.
 */

public class BaseErrorDisposableObservable<T,E> extends DisposableObserver<T> {
    @Override
    public void onNext(T responsePojo) {

    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException){
            Response<?> response = ((HttpException) e).response();
            ResponseBody responseBody = response.errorBody();
        }
    }

    @Override
    public void onComplete() {

    }
}
