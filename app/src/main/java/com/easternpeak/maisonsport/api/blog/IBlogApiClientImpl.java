package com.easternpeak.maisonsport.api.blog;

import com.easternpeak.maisonsport.api.models.Blog;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by antonandreev on 26.02.18.
 */

public class IBlogApiClientImpl implements IBlogApiClient {

    private final IBlogApi api;

    public IBlogApiClientImpl(Retrofit retrofit) {
        this.api = retrofit.create(IBlogApi.class);
    }

    @Override
    public Observable<List<Blog>> getBlogs(int type) {
        return api.getBlogs(type)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Blog> getBlog(long blogId) {
        return api.getBlog(blogId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> sendComment(String message, long blogId) {
        return api.sendComment(message,blogId)
                .map(new Function<Response<ResponseBody>, Boolean>() {
                    @Override
                    public Boolean apply(Response<ResponseBody> responseBodyResponse) throws Exception {
                        return responseBodyResponse.isSuccessful();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
