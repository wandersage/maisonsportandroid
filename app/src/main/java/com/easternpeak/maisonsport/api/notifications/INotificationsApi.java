package com.easternpeak.maisonsport.api.notifications;

import com.easternpeak.maisonsport.api.models.Notification;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by antonandreev on 12.03.18.
 */

interface INotificationsApi {
    @GET("user/notifications")
    Observable<List<Notification>> getNotifications();
    @POST("user/remove/notification/{id}")
    Observable<Response<ResponseBody>> removeNotifications(@Path("id") long id);
}
