package com.easternpeak.maisonsport.api.blog;

import com.easternpeak.maisonsport.api.models.Blog;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by antonandreev on 26.02.18.
 */

public interface IBlogApiClient {
    Observable<List<Blog>> getBlogs(int type);
    Observable<Blog> getBlog(long blogId);

    Observable<Boolean> sendComment(String message,long blogId);
}
