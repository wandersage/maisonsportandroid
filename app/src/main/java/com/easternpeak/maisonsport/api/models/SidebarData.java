package com.easternpeak.maisonsport.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SidebarData extends Booking {

    public static enum SidebarDataType {
        booking, extended_message
    }

    @SerializedName("data_type")
    @Expose
    private SidebarDataType dataType;
    @SerializedName("sport")
    @Expose
    private String sport;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("ability_level")
    @Expose
    private String abilityLevel;
    @SerializedName("age_of_skiers")
    @Expose
    private String ageOfSkiers;

    public SidebarDataType getDataType() {
        return dataType;
    }

    public void setDataType(SidebarDataType dataType) {
        this.dataType = dataType;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAbilityLevel() {
        return abilityLevel;
    }

    public void setAbilityLevel(String abilityLevel) {
        this.abilityLevel = abilityLevel;
    }

    public String getAgeOfSkiers() {
        return ageOfSkiers;
    }

    public void setAgeOfSkiers(String ageOfSkiers) {
        this.ageOfSkiers = ageOfSkiers;
    }
}
