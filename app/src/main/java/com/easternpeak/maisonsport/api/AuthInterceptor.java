package com.easternpeak.maisonsport.api;


import android.accounts.Account;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.easternpeak.maisonsport.BuildConfig;
import com.easternpeak.maisonsport.di.modules.RetrofitModule;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anton on 24.07.17.
 */

public final class AuthInterceptor implements Interceptor {

//    Interceptor interceptor = new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG
//            ? HttpLoggingInterceptor.Level.BODY
//            : HttpLoggingInterceptor.Level.BASIC);

    private PreferenceUtils preferenceUtils;

    public AuthInterceptor( PreferenceUtils preferenceUtils) {
        this.preferenceUtils = preferenceUtils;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        request = request.newBuilder()
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Accept", "application/json")
                .build();
        if (request.url().encodedPath().contains("login")
                || request.url().encodedPath().contains("forgot-password")
//                || request.url().encodedPath().contains("logout")
                ) {

        } else {
            if (BuildConfig.DEBUG) {
                Log.d("Authorization", request.url().toString());
                Log.d("Authorization", preferenceUtils.getTokenType() + " " + preferenceUtils.getToken());
            }
            request = request.newBuilder()
                    .addHeader("Authorization", preferenceUtils.getTokenType() + " " + preferenceUtils.getToken())
                    .build();
        }
        return chain.proceed(request);
    }

//    @Nullable
//    private String sendRefreshToken() throws IOException {
//        Request refreshRequest = getRefreshRequest();
//        Response response = client.newCall(refreshRequest).execute();
//        if (response.isSuccessful() && response.body() != null) {
//            LoginResponse loginResponse = gson.fromJson(response.body().charStream(), LoginResponse.class);
//            preferenceUtils.setLoginData(loginResponse);
//            Request accountRequest = new Request.Builder()
//                    .get()
//                    .url((BuildConfig.FLAVOR.equals("stage") ? RetrofitModule.STAGE_URL : RetrofitModule.PROD_URL) + "account")
//                    .addHeader("Authorization", "Bearer " + loginResponse.getAccessToken())
//                    .build();
//            Response accountResponse = client.newCall(accountRequest).execute();
//            if (accountResponse.isSuccessful())
//                preferenceUtils.setUserData(gson.fromJson(accountResponse.body().charStream(), Account.class));
//            return loginResponse.getAccessToken();
//        }
//        return null;
//    }
//
//    private Request getRefreshRequest() {
//        Request.Builder builder = new Request.Builder();
//        HttpUrl httpUrl = HttpUrl.parse((BuildConfig.FLAVOR.equals("stage") ? RetrofitModule.STAGE_URL : RetrofitModule.PROD_URL) + "login");
//        builder.url(httpUrl)
//                .addHeader("Authorization", getLoginBasicHeader())
//                .post(RequestBody.create(MediaType.parse("application/json"), "{\n" +
//                        "  \"refreshToken\":\"" + (preferenceUtils.getLoginData() != null ? preferenceUtils.getLoginData().getRefreshToken() : "") + "\",\n" +
//                        "  \"grantType\" : \"refresh_token\"\n" +
//                        "}"));
//        return builder.build();
//    }

}
