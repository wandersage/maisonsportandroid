package com.easternpeak.maisonsport.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by antonandreev on 23.02.18.
 */

public class LastMessageInThread {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("from_user")
    @Expose
    private long fromUser;
    @SerializedName("thread_id")
    @Expose
    private long threadId;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("filtered_content")
    @Expose
    private String filteredContent;
    @SerializedName("read_at")
    @Expose
    private String readAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private DateAt deletedAt;
}
