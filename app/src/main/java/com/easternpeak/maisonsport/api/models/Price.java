
package com.easternpeak.maisonsport.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("cost_of_booking")
    @Expose
    private PriceType costOfBooking;
    @SerializedName("ms_fees")
    @Expose
    private PriceType msFees;
    @SerializedName("total")
    @Expose
    private PriceType total;

    public PriceType getCostOfBooking() {
        return costOfBooking;
    }

    public PriceType getMsFees() {
        return msFees;
    }

    public PriceType getTotal() {
        return total;
    }

    public static class PriceType {
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("price")
        @Expose
        private String price;

        public String getPrice() {
            return price;
        }

        public String getDescription() {

            return description;
        }
    }

}
