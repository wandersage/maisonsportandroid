package com.easternpeak.maisonsport.api.notifications;

import com.easternpeak.maisonsport.api.models.Notification;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by antonandreev on 12.03.18.
 */

public interface INotificationsApiClient {
    Observable<List<Notification>> getNotifications();
    Observable<Response<ResponseBody>> removeNotifications(long id);
}
