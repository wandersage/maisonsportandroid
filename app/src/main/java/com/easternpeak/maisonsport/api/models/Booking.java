
package com.easternpeak.maisonsport.api.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Booking {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("resort")
    @Expose
    private String resort;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("slots")
    @Expose
    private List<String> slots = null;
    @SerializedName("price")
    @Expose
    private Price price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getResort() {
        return resort;
    }

    public void setResort(String resort) {
        this.resort = resort;
    }

    public List<String> getSlots() {
        return slots;
    }

    public void setSlots(List<String> slots) {
        this.slots = slots;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }
}
