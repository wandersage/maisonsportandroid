
package com.easternpeak.maisonsport.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentUser {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("mangopay_id")
    @Expose
    private long mangopayId;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("username")
    @Expose
    private Object username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("preferred_language")
    @Expose
    private String preferredLanguage;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("was_manually_verified")
    @Expose
    private boolean wasManuallyVerified;
    @SerializedName("has_submitted_valid_kyc_documents")
    @Expose
    private boolean hasSubmittedValidKycDocuments;
    @SerializedName("has_confirmed_email_address")
    @Expose
    private boolean hasConfirmedEmailAddress;
    @SerializedName("is_admin")
    @Expose
    private boolean isAdmin;
    @SerializedName("confirmation_token")
    @Expose
    private String confirmationToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("instructor_profile_id")
    @Expose
    private Object instructorProfileId;
    @SerializedName("is_suspended")
    @Expose
    private boolean isSuspended;
    @SerializedName("reactivation_token")
    @Expose
    private Object reactivationToken;
    @SerializedName("first_name_passport")
    @Expose
    private Object firstNamePassport;
    @SerializedName("last_name_passport")
    @Expose
    private Object lastNamePassport;
    @SerializedName("carte_prof_number")
    @Expose
    private Object carteProfNumber;
    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("role_id")
    @Expose
    private long roleId;
    @SerializedName("profilepicture")
    @Expose
    private String profilepicture;
    @SerializedName("referred_id")
    @Expose
    private Object referredId;
    @SerializedName("last_activity")
    @Expose
    private String lastActivity;
    @SerializedName("public_name")
    @Expose
    private String publicName;
    @SerializedName("initials")
    @Expose
    private String initials;
    @SerializedName("instructor_profile")
    @Expose
    private Object instructorProfile;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMangopayId() {
        return mangopayId;
    }

    public void setMangopayId(long mangopayId) {
        this.mangopayId = mangopayId;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Object getUsername() {
        return username;
    }

    public void setUsername(Object username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getAvatar() {
        return avatar;
    }


    public Object getMiddleName() {
        return middleName;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isWasManuallyVerified() {
        return wasManuallyVerified;
    }

    public void setWasManuallyVerified(boolean wasManuallyVerified) {
        this.wasManuallyVerified = wasManuallyVerified;
    }

    public boolean isHasSubmittedValidKycDocuments() {
        return hasSubmittedValidKycDocuments;
    }

    public void setHasSubmittedValidKycDocuments(boolean hasSubmittedValidKycDocuments) {
        this.hasSubmittedValidKycDocuments = hasSubmittedValidKycDocuments;
    }

    public boolean isHasConfirmedEmailAddress() {
        return hasConfirmedEmailAddress;
    }

    public void setHasConfirmedEmailAddress(boolean hasConfirmedEmailAddress) {
        this.hasConfirmedEmailAddress = hasConfirmedEmailAddress;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Object getInstructorProfileId() {
        return instructorProfileId;
    }

    public void setInstructorProfileId(Object instructorProfileId) {
        this.instructorProfileId = instructorProfileId;
    }

    public boolean isIsSuspended() {
        return isSuspended;
    }

    public void setIsSuspended(boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public Object getReactivationToken() {
        return reactivationToken;
    }

    public void setReactivationToken(Object reactivationToken) {
        this.reactivationToken = reactivationToken;
    }

    public Object getFirstNamePassport() {
        return firstNamePassport;
    }

    public void setFirstNamePassport(Object firstNamePassport) {
        this.firstNamePassport = firstNamePassport;
    }

    public Object getLastNamePassport() {
        return lastNamePassport;
    }

    public void setLastNamePassport(Object lastNamePassport) {
        this.lastNamePassport = lastNamePassport;
    }

    public Object getCarteProfNumber() {
        return carteProfNumber;
    }

    public void setCarteProfNumber(Object carteProfNumber) {
        this.carteProfNumber = carteProfNumber;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getProfilepicture() {
        return profilepicture;
    }

    public void setProfilepicture(String profilepicture) {
        this.profilepicture = profilepicture;
    }

    public Object getReferredId() {
        return referredId;
    }

    public void setReferredId(Object referredId) {
        this.referredId = referredId;
    }

    public String getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getPublicName() {
        return publicName;
    }

    public void setPublicName(String publicName) {
        this.publicName = publicName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Object getInstructorProfile() {
        return instructorProfile;
    }

    public void setInstructorProfile(Object instructorProfile) {
        this.instructorProfile = instructorProfile;
    }

}
