package com.easternpeak.maisonsport.api.login;

import com.easternpeak.maisonsport.api.models.SignInResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by antonandreev on 21.02.18.
 */

public interface ILoginApiClient {

    Observable<Boolean> signIn(String email, String password);

    Observable<Response<ResponseBody>> signOut();

    Observable<Response<ResponseBody>> forgotPassword(String email);

    Observable<Boolean> registerPush(String refreshedToken);

    Observable<Boolean> unregisterPush();
}
