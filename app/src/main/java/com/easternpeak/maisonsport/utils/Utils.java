package com.easternpeak.maisonsport.utils;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.format.DateUtils;
import android.util.Log;
import android.webkit.WebView;

import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;

import java.io.File;
import java.util.Date;
import java.util.prefs.Preferences;

import okhttp3.internal.Util;

/**
 * Created by antonandreev on 26.02.18.
 */

public class Utils {

    public static void setWebView(WebView webView, String text) {
        if (webView == null)
            return;
        int linkColorInt = Utils.getColor(R.color.blue);
        String linkColor = "#" + Integer.toHexString(linkColorInt & 0x00FFFFFF);
        String textColor = "#" + Integer.toHexString(getColor(R.color.black) & 0x00FFFFFF);

        String doc = "<!DOCTYPE HTML>\n<html>\n<head>\n<style>\n"
                + "body, html { font-family: 'normal'; font-size:"
                + 16 + "px; color:" + textColor + ";}\n"
//                + "img { vertical-align: middle;}"
                + "a {color:" + linkColor + ";}\n"
                + " p {font-size: " + 16 + "px;}\n"
                + "img{display: inline; height: auto; max-width: 100%;}"
                + "</style>\n</head>\n<body>" + text + "</body>\n</html>";
//        doc = doc.replaceAll("justify", "left");
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setAppCachePath(webView.getContext().getCacheDir().getAbsolutePath());
        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setDefaultTextEncodingName(Xml.Encoding.UTF_8.toString());
        webView.setBackgroundColor(getColor(android.R.color.transparent));
        webView.loadDataWithBaseURL("https://vodiy.ua", doc, "text/html", "utf-8", null);
    }

    public static int getColor(int colorId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return MaisonSportApplication.getInstance().getResources().getColor(colorId, MaisonSportApplication.getInstance().getTheme());
        } else return MaisonSportApplication.getInstance().getResources().getColor(colorId);
    }

    public static Drawable getDrawable(int drawableId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return MaisonSportApplication.getInstance().getResources().getDrawable(drawableId, MaisonSportApplication.getInstance().getTheme());
        } else return MaisonSportApplication.getInstance().getResources().getDrawable(drawableId);
    }

    public static int clearCacheFolder(final File dir, final int numDays) {

        int deletedFiles = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {

                    //first delete subdirectories recursively
                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child, numDays);
                    }

                    //then delete the files and subdirectories in this dir
                    //only empty directories can be deleted, so subdirs have been done first
                    if (child.lastModified() < new Date().getTime() - numDays * DateUtils.DAY_IN_MILLIS) {
                        if (child.delete()) {
                            deletedFiles++;
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(Utils.class.getSimpleName(), String.format("Failed to clean the cache, error %s", e.getMessage()));
            }
        }
        return deletedFiles;
    }
}
