package com.easternpeak.maisonsport.utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by antonandreev on 21.02.18.
 */

public class PreferenceUtils extends TinyDB {
    private static final String TOKEN_KEY = "token_key";
    private static final String TOKEN_TYPE_KEY = "token_type_key";
    private static final String PUSH_TOKEN_KEY = "push_token_key";
    private static final String LANG_KEY = "lang_key";
    private static final String TAG = "PreferenceUtils";
    private static final String TYPE_KEY = "user_type_key";

    public PreferenceUtils(Context appContext) {
        super(appContext);
    }

    public void setToken(String accessToken) {
        putString(TOKEN_KEY, accessToken);
    }

    public String getToken() {
        return getString(TOKEN_KEY);
    }

    public void setTokenType(String tokenType) {
        putString(TOKEN_TYPE_KEY, tokenType);
    }

    public boolean isLogged() {
        return getString(TOKEN_KEY) != null;
    }

    public String getTokenType() {
        return getString(TOKEN_TYPE_KEY);
    }

    public void setPushToken(String refreshedToken) {
        putString(PUSH_TOKEN_KEY, refreshedToken);
    }

    @Nullable
    public String getPushToken() {
        return getString(PUSH_TOKEN_KEY);
    }

    @Nullable
    public String getLanguage() {
        String lang = getString(LANG_KEY);
        Log.d(TAG, "get " + lang);
        return lang;

    }

    public void setLanguage(String lang) {
        if (lang.equals("it"))
            lang = "en";
        putString(LANG_KEY, lang);
        Log.d(TAG, "set " + lang);

    }

    public static String getLanguage(Context base) {
        String lang = PreferenceManager.getDefaultSharedPreferences(base).getString(LANG_KEY, "en");
        Log.d(TAG, "static get " + lang);
        return lang;
    }

    public boolean isInstructor() {
        return !"customer".equals(getString(TYPE_KEY));
    }

    public void setType(String type) {
        putString(TYPE_KEY, type);
    }
}
