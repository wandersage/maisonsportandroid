package com.easternpeak.maisonsport.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Blog;
import com.easternpeak.maisonsport.api.models.Comment;
import com.easternpeak.maisonsport.fcm.MyFirebaseMessagingService;
import com.easternpeak.maisonsport.mvp.presenters.BlogPresenter;
import com.easternpeak.maisonsport.mvp.views.IBlogView;
import com.easternpeak.maisonsport.ui.activities.BaseActivity;
import com.easternpeak.maisonsport.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by antonandreev on 26.02.18.
 */

public class BlogFragment extends BaseProgressFragment implements IBlogView {

    private static final String BLOG_KEY = "blog_key";
    private static final String BLOG_ID_KEY = "blog_id_key";
    @InjectPresenter
    BlogPresenter blogPresenter;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.createDate)
    TextView createDate;
    //    @BindView(R.id.image)
//    ImageView image;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.add_comment_btn)
    Button addCommentBtn;
    @BindView(R.id.progress_view)
    FrameLayout progressView;
    @BindView(R.id.comentsLayout)
    LinearLayout commentsLayout;
    Unbinder unbinder;
    @BindView(R.id.comment_et)
    EditText commentEt;
    private Blog blog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @ProvidePresenter
    BlogPresenter provideBlogPresenter() {
        Bundle bundle = getArguments();
        blog = bundle.getParcelable(BLOG_KEY);
        if (blog != null) {
            return new BlogPresenter(blog.getId());
        } else
            return new BlogPresenter(bundle.getLong(BLOG_ID_KEY));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (blog != null)
            setViews(blog);

        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BaseActivity activity= (BaseActivity) getActivity();
        if (activity!=null){
            ActionBar actionBar = activity.getSupportActionBar();
            if (actionBar!=null)
                actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private BroadcastReceiver updatesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED.equals(intent.getAction()) && intent.hasExtra("category_id")) {
                String threadExtraIdStr = intent.getStringExtra("category_id");
                final long threadExtraId = Long.parseLong(threadExtraIdStr);
                if (blog != null && blog.getId() == threadExtraId) {
                    blogPresenter.getBlog(threadExtraId);
                    removeNotification(intent.getIntExtra(MyFirebaseMessagingService.EXTRA_NOTIFICATION_ID, 0));
                }
            }

        }
    };

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(updatesReceiver, new IntentFilter(MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(updatesReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected View getProgressView() {
        return progressView;
    }

    @OnClick(R.id.add_comment_btn)
    public void onViewClicked() {
        blogPresenter.addComment(commentEt.getText().toString());
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(commentEt.getWindowToken(), 0);
        }
    }

    @Override
    public void showProgress() {
        super.showProgress();
        addCommentBtn.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        addCommentBtn.setEnabled(true);
    }

    public static BlogFragment newInstance(Blog blog) {
        BlogFragment blogFragment = new BlogFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(BLOG_KEY, blog);
        blogFragment.setArguments(bundle);
        return blogFragment;
    }

    @Override
    public void onLoaded(Blog blog) {
        setViews(blog);
    }

    @Override
    public void onCommentSent() {
        commentEt.setText("");
    }

    private void setViews(Blog blog) {
        createDate.setText(blog.getDateAndMinRead());
        title.setText(blog.getTitle());
        Utils.setWebView(webView, blog.getContent());
        if (blog.getComment() != null) {
            commentsLayout.removeAllViews();
            for (Comment comment : blog.getComment()) {
                View view = getLayoutInflater().inflate(R.layout.adapter_blog_comment, commentsLayout, false);
                ViewHolder holder = new ViewHolder(view);
                holder.text.setText(comment.getContent());
                view.setTag(holder);
                commentsLayout.addView(view);
            }
        }
    }

    @NonNull
    public static BlogFragment newInstance(long toLong) {
        BlogFragment blogFragment = new BlogFragment();
        Bundle bundle = new Bundle(1);
        bundle.putLong(BLOG_ID_KEY, toLong);
        blogFragment.setArguments(bundle);
        return blogFragment;
    }

    static class ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.text)
        TextView text;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
