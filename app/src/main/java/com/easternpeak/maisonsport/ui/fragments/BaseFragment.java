package com.easternpeak.maisonsport.ui.fragments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.easternpeak.maisonsport.utils.PreferenceUtils;

import java.util.Locale;
import java.util.prefs.Preferences;

/**
 * Created by antonandreev on 26.02.18.
 */

public class BaseFragment extends MvpAppCompatFragment {
    public interface FragmentManagerHandler {
        void add(Fragment fragment, boolean isBackStack);

        void replace(Fragment fragment, boolean isBackStack);
    }

    private FragmentManagerHandler fragmentManagerHandler;

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof FragmentManagerHandler)
            fragmentManagerHandler = (FragmentManagerHandler) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLocale(context.getApplicationContext());
    }

    protected void removeNotification(int notificationId) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel(notificationId);
        }
    }

    public void setLocale(Context context) {
        Locale locale = new Locale(PreferenceUtils.getLanguage(context));
        Configuration config = getResources().getConfiguration();
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M
                && Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(locale);
            config.setLocale(locale);
        }else config.setLocale(locale);
        this.getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    public FragmentManagerHandler getFragmentManagerHandler() {
        return fragmentManagerHandler;
    }
}
