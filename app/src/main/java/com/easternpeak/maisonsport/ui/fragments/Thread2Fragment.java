package com.easternpeak.maisonsport.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Booking;
import com.easternpeak.maisonsport.api.models.MessageThread;
import com.easternpeak.maisonsport.api.models.SidebarData;
import com.easternpeak.maisonsport.fcm.MyFirebaseMessagingService;
import com.easternpeak.maisonsport.mvp.presenters.ThreadPresenter;
import com.easternpeak.maisonsport.mvp.views.ThreadView;
import com.easternpeak.maisonsport.ui.activities.BaseActivity;
import com.easternpeak.maisonsport.ui.adapters.MessagesAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by antonandreev on 05.03.18.
 */

public class Thread2Fragment extends BaseProgressFragment implements ThreadView {
    private static final String THREAD_ID_EXTRA = "thread_id_extra";
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.list)
    ListView messageList;
    //    @BindView(R.id.swipeRefreshLayout)
//    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.send_message)
    ImageButton sendMessage;
    Unbinder unbinder;

    @InjectPresenter
    ThreadPresenter presenter;

    @BindView(R.id.showBooking)
    CheckedTextView showBooking;
    @BindView(R.id.reference_number)
    TextView referenceNumber;
    @BindView(R.id.resort)
    TextView resort;
    @BindView(R.id.date_times)
    TextView dateTimes;
    @BindView(R.id.pay1)
    TextView pay1;
    @BindView(R.id.pay1_price)
    TextView pay1Price;
    @BindView(R.id.pay2)
    TextView pay2;
    @BindView(R.id.pay2_price)
    TextView pay2Price;
    @BindView(R.id.total_vat)
    TextView totalVat;
    @BindView(R.id.total_vat_price)
    TextView totalVatPrice;
    @BindView(R.id.view_details)
    Button viewDetails;
    @BindView(R.id.booking_info)
    LinearLayout bookingInfo;
    @BindView(R.id.internalEmpty)
    TextView internalEmpty;
    @BindView(R.id.listContainer)
    FrameLayout listContainer;
    @BindView(R.id.progressContainer)
    LinearLayout progressContainer;
    @BindView(R.id.resort_extended)
    TextView resortExtended;
    @BindView(R.id.sport_extended)
    TextView sportExtended;
    @BindView(R.id.date_extended)
    TextView dateExtended;
    @BindView(R.id.ability_level_extended)
    TextView abilityLevelExtended;
    @BindView(R.id.age_extended)
    TextView ageExtended;
    @BindView(R.id.extended_info)
    LinearLayout extendedInfo;
    private long threadId = -1;
    private SidebarData.SidebarDataType sidebarDataType;

    public static Thread2Fragment newInstance(long threadId) {
        Bundle bundle = new Bundle();
        bundle.putLong(THREAD_ID_EXTRA, threadId);
        Thread2Fragment fragment = new Thread2Fragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        threadId = getArguments().getLong(THREAD_ID_EXTRA, -1);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thread2, container, false);
        unbinder = ButterKnife.bind(this, view);
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                swipeRefreshLayout.setRefreshing(true);
//                presenter.getThread(threadId);
//            }
//        });
        presenter.getThread(threadId);
        return view;
    }

    private BroadcastReceiver updatesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED.equals(intent.getAction()) && intent.hasExtra("thread_id")) {
                String threadExtraIdStr = intent.getStringExtra("thread_id");
                final long threadExtraId = Long.parseLong(threadExtraIdStr);
                if (threadId == threadExtraId) {
                    presenter.getThread(threadId);
                    removeNotification(intent.getIntExtra(MyFirebaseMessagingService.EXTRA_NOTIFICATION_ID, 0));
                }
            }

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getSupportActionBar();
            if (actionBar != null)
                actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(updatesReceiver, new IntentFilter(MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(updatesReceiver);
    }

    @Override
    public void showProgress() {
        super.showProgress();
        internalEmpty.setVisibility(View.GONE);
//        if (!swipeRefreshLayout.isRefreshing()) {
//            listContainer.setVisibility(View.INVISIBLE);
//        }
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        listContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String message) {
        super.onError(message);
        sendMessage.setEnabled(true);
    }

    @Override
    public void onLoaded(MessageThread messageThread) {
        if (messageThread.getRespondentChat().getAvatar() != null)
            Picasso.with(getActivity()).load(messageThread.getRespondentChat().getAvatar()).into(image);
        name.setText(messageThread.getRespondentChat().getPublicName());
        messageList.setAdapter(new MessagesAdapter(messageThread));
        if (messageThread.getSidebarData() != null) {
            sidebarDataType = messageThread.getSidebarData().getDataType();
            if (messageThread.getSidebarData().getDataType() == SidebarData.SidebarDataType.booking) {
                Booking booking = messageThread.getSidebarData();
                showBooking.setVisibility(View.VISIBLE);
                showBooking.setText(booking.getNotification());
                referenceNumber.setText(getString(R.string.reference_number, booking.getId()));
                resort.setText(booking.getResort());
                StringBuilder stringBuilder = new StringBuilder();
                for (String slot : booking.getSlots())
                    stringBuilder.append(slot).append("\n");
                dateTimes.setText(stringBuilder.toString());
                pay1.setText(booking.getPrice().getCostOfBooking().getDescription());
                pay2.setText(booking.getPrice().getMsFees().getDescription());
                pay1Price.setText(booking.getPrice().getCostOfBooking().getPrice());
                pay2Price.setText(booking.getPrice().getMsFees().getPrice());
                totalVat.setText(booking.getPrice().getTotal().getDescription());
                totalVatPrice.setText(booking.getPrice().getTotal().getPrice());
            } else if (messageThread.getSidebarData().getDataType() == SidebarData.SidebarDataType.extended_message) {
                SidebarData sidebarData = messageThread.getSidebarData();
                resortExtended.setText(sidebarData.getResort());
                sportExtended.setText(sidebarData.getSport());
                dateExtended.setText(sidebarData.getDate());
                abilityLevelExtended.setText(sidebarData.getAbilityLevel());
                ageExtended.setText(sidebarData.getAgeOfSkiers());
                showBooking.setVisibility(View.VISIBLE);
                showBooking.setText(R.string.extended_info);
            }
        }
        if (messageThread.getMessages().isEmpty())
            internalEmpty.setVisibility(View.VISIBLE);
        else {
            messageList.setSelection(messageList.getAdapter().getCount() - 1);
        }
    }

    @Override
    public void onMessageSent() {
        sendMessage.setEnabled(true);
        message.setText("");
        presenter.getThread(threadId);
    }

    @Override
    protected View getProgressView() {
        return progressContainer;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.showBooking)
    public void onViewClicked() {
        showBooking.setChecked(!showBooking.isChecked());
        if (sidebarDataType == SidebarData.SidebarDataType.booking)
            bookingInfo.setVisibility(showBooking.isChecked() ? View.VISIBLE : View.GONE);
        else if (sidebarDataType == SidebarData.SidebarDataType.extended_message)
            extendedInfo.setVisibility(showBooking.isChecked() ? View.VISIBLE : View.GONE);

    }

    @OnClick(R.id.send_message)
    public void onMessageSend() {
        presenter.sendMessage(threadId, message.getText().toString());
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(message.getWindowToken(), 0);
        }
    }
}
