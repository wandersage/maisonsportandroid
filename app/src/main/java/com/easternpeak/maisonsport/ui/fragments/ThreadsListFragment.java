package com.easternpeak.maisonsport.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Thread;
import com.easternpeak.maisonsport.fcm.MyFirebaseMessagingService;
import com.easternpeak.maisonsport.mvp.presenters.MessagesListPresenter;
import com.easternpeak.maisonsport.mvp.views.MessagesListView;
import com.easternpeak.maisonsport.ui.activities.MainActivity;
import com.easternpeak.maisonsport.ui.adapters.MessageThreadAdapter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 22.02.18.
 */

public class ThreadsListFragment extends BaseListFragment implements MessagesListView, AdapterView.OnItemClickListener {

    @InjectPresenter
    MessagesListPresenter presenter;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("thread_id")) {
                String threadIdSgtr = intent.getStringExtra("thread_id");
                long threadId = Long.parseLong(threadIdSgtr);
                if (budgetMap.get(threadId) != null)
                    budgetMap.put(threadId, budgetMap.get(threadId) + 1);
                else budgetMap.put(threadId, 1);
                if (adapter != null)
                    adapter.notifyDataSetChanged();
            }
        }
    };

    private HashMap<Long, Integer> budgetMap = new LinkedHashMap<>(0);
    private MessageThreadAdapter adapter;

    @Override
    public void onMessageLoaded(List<Thread> messageThreads) {
        if (swipeRefreshLayout.isRefreshing())
            budgetMap.clear();
        adapter = new MessageThreadAdapter(messageThreads, budgetMap);
        list.setAdapter(adapter);
        internalEmpty.setVisibility(messageThreads.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        budgetMap.remove(l);
        if (adapter != null)
            adapter.notifyDataSetChanged();
        getFragmentManagerHandler().add(Thread2Fragment.newInstance(l), true);
    }

    @Override
    public void onRefresh() {
        presenter.getMessages();
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(receiver, new IntentFilter(MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(receiver);
    }

}
