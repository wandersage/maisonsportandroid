package com.easternpeak.maisonsport.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Notification;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by antonandreev on 12.03.18.
 */

public class NotificationsAdapter extends BaseAdapter {
    private final List<Notification> list;

    public NotificationsAdapter(List<Notification> notifications) {
        this.list = notifications;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_notification, viewGroup, false);
            view.setTag(holder = new ViewHolder(view));
        } else holder = (ViewHolder) view.getTag();
        Notification notification = list.get(i);
        holder.date.setText(notification.getCreatedAt());
        holder.text.setText(notification.getMessage());
        holder.title.setText(notification.getTitle());
        if (notification.getAvatar() != null)
            Picasso.with(view.getContext()).load(notification.getAvatar()).into(holder.image);
        return view;
    }

    public void remove(Notification notification) {
        list.remove(notification);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.date)
        TextView date;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
