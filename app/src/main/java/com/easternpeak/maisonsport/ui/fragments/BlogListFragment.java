package com.easternpeak.maisonsport.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Blog;
import com.easternpeak.maisonsport.mvp.presenters.BlogListPresenter;
import com.easternpeak.maisonsport.mvp.views.IBlogListView;
import com.easternpeak.maisonsport.ui.adapters.BlogAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 22.02.18.
 */

public class BlogListFragment extends BaseListFragment implements IBlogListView, AdapterView.OnItemClickListener {
    @InjectPresenter
    BlogListPresenter blogPresenter;

    @Override
    public void onLoaded(List<Blog> blogs) {
        internalEmpty.setVisibility(blogs.isEmpty() ? View.VISIBLE : View.GONE);
        list.setAdapter(new BlogAdapter(blogs));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        getFragmentManagerHandler().add(BlogFragment.newInstance((Blog) list.getItemAtPosition(i)),true);
    }

    @Override
    public void onRefresh() {
        blogPresenter.getBlogs();
    }
}
