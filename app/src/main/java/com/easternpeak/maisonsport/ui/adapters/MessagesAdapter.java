package com.easternpeak.maisonsport.ui.adapters;

import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Message;
import com.easternpeak.maisonsport.api.models.MessageThread;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by antonandreev on 05.03.18.
 */

public class MessagesAdapter extends BaseAdapter {
    private final MessageThread thread;

    public MessagesAdapter(MessageThread messageThread) {
        this.thread = messageThread;
    }

    @Override
    public int getCount() {
        return thread.getMessages().size();
    }

    @Override
    public Object getItem(int i) {
        return thread.getMessages().get(i);
    }

    @Override
    public long getItemId(int i) {
        return thread.getMessages().get(i).hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = thread.getMessages().get(position);
        if (message.getUserId() == thread.getCurrentUser().getId())
            return 0;
        else if (message.getUserId() == thread.getRespondentChat().getId())
            return 1;
        else return 2;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            switch (getItemViewType(i)) {
                case 0:
                    view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_message_outgoing, viewGroup, false);
                    break;
                case 1:
                    view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_message_incoming, viewGroup, false);
                    break;
                default:
                    view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_messaging_booking, viewGroup, false);
                    break;
            }
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else holder = (ViewHolder) view.getTag();
        Message message = thread.getMessages().get(i);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.text.setText(Html.fromHtml(message.getContent(), Html.FROM_HTML_MODE_LEGACY));
        } else holder.text.setText(Html.fromHtml(message.getContent()));

        try {
            Date serverDate = serverFormat.parse(message.getCreatedAt());
            serverDate = new Date(serverDate.getTime() + TimeZone.getDefault().getRawOffset());
            if (serverDate.before(new Date(Calendar.getInstance().getTimeInMillis() - TimeUnit.DAYS.toMillis(1))))
                holder.dateTime.setText(dateFormat.format(serverDate));
            else holder.dateTime.setText(timeFormat.format(serverDate));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return view;
    }

    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM", Locale.ENGLISH);
    private static final SimpleDateFormat serverFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);

    static class ViewHolder {
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.dateTime)
        TextView dateTime;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
