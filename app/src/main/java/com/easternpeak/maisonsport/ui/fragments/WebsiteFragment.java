package com.easternpeak.maisonsport.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.easternpeak.maisonsport.BuildConfig;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.di.modules.RetrofitModule;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 22.02.18.
 */

public class WebsiteFragment extends Fragment {
    private static final String EXTRA_URL = "url_extra";
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.progress_view)
    FrameLayout progressView;
    Unbinder unbinder;
    private WebViewClient webViewClientAPI14 = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (progressView != null)
                progressView.setVisibility(View.VISIBLE);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (progressView != null)
                progressView.setVisibility(View.GONE);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        Context context = getActivity() != null ? getActivity() : getContext();
        if (context != null)
            settings.setAppCachePath(context.getCacheDir().getAbsolutePath());
        webView.setWebViewClient(webViewClientAPI14);
        String url = null;
        if (savedInstanceState != null)
            url = savedInstanceState.getString(EXTRA_URL);
        else if (getArguments() != null)
            url = getArguments().getString(EXTRA_URL);
        webView.loadUrl(url != null ? url : (BuildConfig.FLAVOR.equals("stage") ? RetrofitModule.STAGE_URL : RetrofitModule.PROD_URL));
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_URL, webView.getUrl());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        webView.stopLoading();
        unbinder.unbind();
    }

    public static WebsiteFragment newInstance(String url) {
        WebsiteFragment fragment = new WebsiteFragment();
        Bundle b = new Bundle(1);
        b.putString(EXTRA_URL, url);
        fragment.setArguments(b);
        return fragment;
    }
}
