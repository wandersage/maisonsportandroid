package com.easternpeak.maisonsport.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Blog;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by antonandreev on 26.02.18.
 */

public class BlogAdapter extends BaseAdapter {
    private final List<Blog> list;

    public BlogAdapter(List<Blog> blogs) {
        this.list = blogs;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    private static final SimpleDateFormat date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
    private static final SimpleDateFormat serverDate = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH);

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_blog, viewGroup, false);
            view.setTag(holder = new ViewHolder(view));
        }else holder= (ViewHolder) view.getTag();
        Blog blog = list.get(i);
        Document document = Jsoup.parse(blog.getContent());
        Element firstImageElement = document.selectFirst("img");
        final String imagePath = firstImageElement!=null ? firstImageElement.absUrl("src") : "";
        if (!imagePath.isEmpty()){
            Picasso.with(viewGroup.getContext()).load(imagePath).into(holder.image);
        }else holder.image.setImageDrawable(null);
        holder.title.setText(blog.getTitle());
        holder.text.setText(document.text());
        try {
            holder.createDate.setText(date.format(serverDate.parse(blog.getCreatedAt())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.createDate.setText(blog.getDateAndMinRead());

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.createDate)
        TextView createDate;
        @BindView(R.id.image)
        ImageView image;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
