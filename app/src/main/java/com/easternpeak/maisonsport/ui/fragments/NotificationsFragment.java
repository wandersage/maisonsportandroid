package com.easternpeak.maisonsport.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Notification;
import com.easternpeak.maisonsport.fcm.MyFirebaseMessagingService;
import com.easternpeak.maisonsport.mvp.presenters.NotificationsPresenter;
import com.easternpeak.maisonsport.mvp.views.INotificationsView;
import com.easternpeak.maisonsport.ui.adapters.NotificationsAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 12.03.18.
 */

public class NotificationsFragment extends BaseListFragment implements INotificationsView, AdapterView.OnItemClickListener {

    @InjectPresenter
    NotificationsPresenter presenter;
    private NotificationsAdapter adapter;

    @Override
    public void onLoaded(List<Notification> notifications) {
        internalEmpty.setVisibility(notifications.isEmpty() ? View.VISIBLE : View.GONE);
        list.setAdapter(adapter = new NotificationsAdapter(notifications));
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
                presenter.getNotifications();
            }
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        internalEmpty.setText(R.string.notifications_list_empty);
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(receiver, new IntentFilter(MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED));
//        presenter.getNotifications();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(receiver);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        presenter.removeNotificaiton(l);
        Notification notification = (Notification) list.getAdapter().getItem(i);
        if ("post".equals(notification.getType()))
            getFragmentManagerHandler().add(BlogFragment.newInstance(notification.getPostId()), true);
        else if ("booking".equals(notification.getType()) || "message".equals(notification.getType()) && notification.getThreadId() != -1)
            getFragmentManagerHandler().add(Thread2Fragment.newInstance(notification.getThreadId()), true);
        adapter.remove(notification);
    }

    @Override
    public void onRefresh() {
        presenter.getNotifications();
    }
}
