package com.easternpeak.maisonsport.ui.activities

import android.content.Intent
import android.content.Intent.parseIntent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.easternpeak.maisonsport.MaisonSportApplication
import com.easternpeak.maisonsport.R
import com.easternpeak.maisonsport.api.login.ILoginApiClient
import com.easternpeak.maisonsport.ui.fragments.*
import com.easternpeak.maisonsport.utils.PreferenceUtils
import javax.inject.Inject

class MainActivity : BaseActivity() {
    override fun getContainerId(): Int {
        return R.id.container;
    }

    @Inject
    lateinit var preferenceUtils: PreferenceUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MaisonSportApplication.getComponent().inject(this);
        setContentView(R.layout.activity_empty)
        setSupportActionBar(findViewById(R.id.toolbar))
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(false)
            actionBar.setDisplayHomeAsUpEnabled(false)
            actionBar.setDisplayShowTitleEnabled(false)
        }
        if (savedInstanceState == null)
            replace(if (preferenceUtils.token!=null) MainTabFragment() else LoginFragment() ,false);
        parseNotificationIntent(intent);
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home) {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        parseNotificationIntent(intent);
    }

    private fun parseNotificationIntent(intent: Intent?) {
        if (intent != null) {
            if (intent.hasExtra("thread_id"))
                add(Thread2Fragment.newInstance(intent.getStringExtra("thread_id").toLong()),true)
            else if (intent.hasExtra("post_id"))
                add(BlogFragment.newInstance(intent.getStringExtra("category_id").toLong()),true)
        }
    }

}
