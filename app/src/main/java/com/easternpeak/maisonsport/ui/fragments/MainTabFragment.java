package com.easternpeak.maisonsport.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Notification;
import com.easternpeak.maisonsport.fcm.MyFirebaseMessagingService;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 22.02.18.
 */

public class MainTabFragment extends Fragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.tabLayout)
    SmartTabLayout tabLayout;
    Unbinder unbinder;
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tabs, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewpager.setOffscreenPageLimit(4);
        tabLayout.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                container = (ViewGroup) LayoutInflater.from(container.getContext()).inflate(R.layout.tab_item, container, false);
                ImageView imageView = container.findViewById(R.id.tab_item_image);
                TextView text = container.findViewById(R.id.tab_item_text);
                switch (position) {
                    case 0:
                        imageView.setImageResource(R.drawable.ic_messages_selector);
                        text.setText(getString(R.string.messages));
                        break;
                    case 1:
                        imageView.setImageResource(R.drawable.ic_blog_selector);
                        text.setText(getString(R.string.blog));
                        break;
                    case 2:
                        imageView.setImageResource(R.drawable.ic_web_selector);
                        text.setText(getString(R.string.website));
                        break;
                    case 3:
                        imageView.setImageResource(R.drawable.ic_notifications_selector);
                        text.setText(getString(R.string.notifications));
                        break;
                    case 4:
                        imageView.setImageResource(R.drawable.ic_settings_selector);
                        text.setText(getString(R.string.settings));
                        break;
                }
                return container;
            }
        });
        FragmentStatePagerItemAdapter adapter = new FragmentStatePagerItemAdapter(getChildFragmentManager(),
                FragmentPagerItems.with(getActivity())
                        .add(R.string.messages, ThreadsListFragment.class)
                        .add(R.string.blog, BlogListFragment.class)
                        .add(R.string.website, WebsiteFragment.class)
                        .add(R.string.notifications, NotificationsFragment.class)
                        .add(R.string.settings, SettingsFragment.class)
                        .create());
        viewpager.setAdapter(adapter);
        tabLayout.setViewPager(viewpager);
        viewpager.addOnPageChangeListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 3) {
            View tabView = tabLayout.getTabAt(position);
            if (tabView != null) {
                ImageView imageView = tabView.findViewById(R.id.tab_item_image);
                imageView.setImageResource(R.drawable.ic_notifications_selector);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
                View tabView = tabLayout.getTabAt(3);
                if (tabView != null && viewpager.getCurrentItem()!=3) {
                    ImageView imageView = tabView.findViewById(R.id.tab_item_image);
                    imageView.setImageResource(R.drawable.ic_bring_red);
                }
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(receiver, new IntentFilter(MyFirebaseMessagingService.ACTION_MESSAGE_RECEIVED));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(receiver);
    }
}
