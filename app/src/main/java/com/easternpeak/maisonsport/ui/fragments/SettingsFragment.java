package com.easternpeak.maisonsport.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.mvp.presenters.LoginPresenter;
import com.easternpeak.maisonsport.mvp.views.LoginView;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 22.02.18.
 */

public class SettingsFragment extends BaseProgressFragment implements LoginView, AdapterView.OnItemSelectedListener {

    @Inject
    PreferenceUtils preferenceUtils;

    @InjectPresenter
    LoginPresenter presenter;

    @BindView(R.id.logout_btn)
    Button logoutBtn;
    @BindView(R.id.enable_notifications)
    SwitchCompat enableNotifications;
    @BindView(R.id.progress_view)
    FrameLayout progressView;

    Unbinder unbinder;
    @BindView(R.id.languageSpinner)
    Spinner languageSpinner;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MaisonSportApplication.getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        if ("en".equals(preferenceUtils.getLanguage()))
            languageSpinner.setSelection(0, false);
        else if ("fr".equals(preferenceUtils.getLanguage()))
            languageSpinner.setSelection(1, false);
//        else if ("it".equals(preferenceUtils.getLanguage()))
//            languageSpinner.setSelection(2, false);
        languageSpinner.setOnItemSelectedListener(this);
        int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());

        if (result!= ConnectionResult.SUCCESS)
            enableNotifications.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && preferenceUtils!=null)
            enableNotifications.setChecked(preferenceUtils.getPushToken() != null);
    }

    @Override
    protected View getProgressView() {
        return progressView;
    }

    @OnClick(R.id.logout_btn)
    public void onViewClicked() {
        presenter.signOut();
    }

    @OnClick(R.id.enable_notifications)
    void onEnableNotifications() {
        if (enableNotifications.isChecked()) {
            presenter.registerPush();
        } else {
            presenter.unregisterPush();
        }
    }

    @Override
    public void showProgress() {
        super.showProgress();
        logoutBtn.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        logoutBtn.setEnabled(true);
    }

    @Override
    public void showEmailError() {

    }

    @Override
    public void showPasswordError() {

    }

    @Override
    public void hideErrors() {

    }

    @Override
    public void onLoginSuccess() {

    }

    @Override
    public void onPushRegistered() {
        enableNotifications.setChecked(true);
    }

    @Override
    public void onError(String message) {
        super.onError(message);
        enableNotifications.setChecked(preferenceUtils.getPushToken() != null);
    }

    @Override
    public void onLogoutSuccess() {
        getFragmentManagerHandler().replace(new LoginFragment(), false);
    }

    @Override
    public void onPushUnRegistered() {
        enableNotifications.setChecked(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                preferenceUtils.setLanguage("en");
                break;
            case 1:
                preferenceUtils.setLanguage("fr");
                break;
//            case 2:
//                preferenceUtils.setLanguage("it");
//                break;
        }
        MaisonSportApplication.getInstance().restart();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
