package com.easternpeak.maisonsport.ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.login.ILoginApiClient;
import com.easternpeak.maisonsport.mvp.presenters.ForgotPasswordPresenter;
import com.easternpeak.maisonsport.mvp.views.IForgotPasswordView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 23.02.18.
 */

public class ForgotPasswordFragment extends BaseProgressFragment implements IForgotPasswordView {

    @InjectPresenter
    ForgotPasswordPresenter presenter;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.progress_view)
    FrameLayout progressView;
    Unbinder unbinder;
    private AlertDialog errorDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.login_btn)
    public void onViewClicked() {
        email.setBackgroundResource(R.drawable.et_bg);
        presenter.forgotPassword(email.getText().toString());
    }

    @Override
    public void showProgress() {
        super.showProgress();
        loginBtn.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        loginBtn.setEnabled(true);
    }

    @Override
    protected View getProgressView() {
        return progressView;
    }

    @Override
    public void onEmailError() {
        email.setBackgroundResource(R.drawable.et_error_bg);
    }

    @Override
    public void onSuccess() {
        getActivity().onBackPressed();
    }
}
