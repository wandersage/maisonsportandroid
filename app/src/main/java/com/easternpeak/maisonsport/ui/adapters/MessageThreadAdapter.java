package com.easternpeak.maisonsport.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.models.Thread;
import com.easternpeak.maisonsport.api.models.RespondentChat;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by antonandreev on 22.02.18.
 */

public class MessageThreadAdapter extends BaseAdapter {

    private static final SimpleDateFormat bookingStartTime = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    private static final SimpleDateFormat serverTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", Locale.getDefault());
    private final List<Thread> threads;
    private final Map<Long, Integer> badgetMap;

    public MessageThreadAdapter(List<Thread> messageThreads, Map<Long, Integer> budgetMap) {
        this.threads = messageThreads;
        this.badgetMap = budgetMap;
    }

    @Override
    public int getCount() {
        return threads.size();
    }

    @Override
    public Object getItem(int i) {
        return threads.get(i);
    }

    @Override
    public long getItemId(int i) {
        return threads.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_message_treads, viewGroup, false);
            view.setTag(holder = new ViewHolder(view));
        } else holder = (ViewHolder) view.getTag();
        Thread thread = threads.get(i);
        RespondentChat respondentChat = thread.getRespondentChat();
        holder.name.setText(respondentChat.getPublicName());
        if (respondentChat.getAvatar() != null)
            Picasso.with(viewGroup.getContext()).load(respondentChat.getAvatar()).into(holder.image);
        else holder.image.setImageResource(R.drawable.img_profile);
        try {
            if (thread.getBookingStart() != null)
                holder.bookingInfo.setText(bookingStartTime.format(serverTime.parse(thread.getBookingStart().getDate()))
                        + " at " + thread.getBookingResort().getName());
            holder.bookingInfo.setVisibility(thread.getBookingResort() != null ? View.VISIBLE : View.GONE);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            holder.date.setText(bookingStartTime.format(serverTime.parse(thread.getActiveAt().getDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (badgetMap.get(thread.getId()) != null) {
            holder.budgetLayout.setVisibility(badgetMap.get(thread.getId()) > 0 ? View.VISIBLE : View.GONE);
            holder.budgetCount.setText(String.valueOf(badgetMap.get(thread.getId())));
        } else
            holder.budgetLayout.setVisibility(View.GONE);

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.booking_info)
        TextView bookingInfo;
        @BindView(R.id.budgetLayout)
        View budgetLayout;
        @BindView(R.id.budgetCount)
        TextView budgetCount;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
