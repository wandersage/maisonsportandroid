package com.easternpeak.maisonsport.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.mvp.views.BaseProgressView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 12.03.18.
 */

public abstract class BaseListFragment extends BaseProgressFragment implements BaseProgressView, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {

    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.internalEmpty)
    TextView internalEmpty;
    @BindView(R.id.listContainer)
    FrameLayout listContainer;
    @BindView(R.id.progressContainer)
    LinearLayout progressContainer;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.swipelist_content, container, false);
        unbinder = ButterKnife.bind(this, view);
        list.setOnItemClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void showProgress() {
        if (!swipeRefreshLayout.isRefreshing())
            super.showProgress();
        internalEmpty.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected View getProgressView() {
        return progressContainer;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
