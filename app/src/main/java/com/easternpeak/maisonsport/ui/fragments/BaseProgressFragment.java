package com.easternpeak.maisonsport.ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.easternpeak.maisonsport.mvp.views.BaseProgressView;

/**
 * Created by antonandreev on 22.02.18.
 */

public abstract class BaseProgressFragment extends BaseFragment implements BaseProgressView {

    protected AlertDialog errorDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void showProgress() {
        View view = getProgressView();
        if (view != null)
            view.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        View view = getProgressView();
        if (view != null)
            view.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        errorDialog = new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (errorDialog != null)
            errorDialog.dismiss();
    }

    protected abstract View getProgressView();

}
