package com.easternpeak.maisonsport.ui.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.easternpeak.maisonsport.BuildConfig;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.mvp.presenters.LoginPresenter;
import com.easternpeak.maisonsport.mvp.views.LoginView;
import com.easternpeak.maisonsport.ui.activities.MainActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by antonandreev on 21.02.18.
 */

public class LoginFragment extends BaseProgressFragment implements LoginView {

    @InjectPresenter
    LoginPresenter loginPresenter;

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.forgot_tv)
    TextView forgotTv;
    Unbinder unbinder;
    @BindView(R.id.progress_view)
    View progressView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MaisonSportApplication.getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (BuildConfig.DEBUG) {
            if ("prod".equals(BuildConfig.FLAVOR))
                email.setText("instructor20@gmail.com");
            else
                email.setText("frCustomer@i.ua");
            password.setText("04820482");
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.login_btn, R.id.forgot_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                loginPresenter.signIn(email.getText().toString(), password.getText().toString());
                break;
            case R.id.forgot_tv:
                getFragmentManagerHandler().add(new ForgotPasswordFragment(), true);
                break;
        }
    }


    @Override
    public void showEmailError() {
        email.setBackgroundResource(R.drawable.et_error_bg);
    }

    @Override
    public void showPasswordError() {
        password.setBackgroundResource(R.drawable.et_error_bg);
    }

    @Override
    public void hideErrors() {
        email.setBackgroundResource(R.drawable.et_bg);
        password.setBackgroundResource(R.drawable.et_bg);
    }

    @Override
    public void onLoginSuccess() {
        int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());
        if (result == ConnectionResult.SUCCESS)
            errorDialog = new AlertDialog.Builder(getActivity())
                    .setMessage(getString(R.string.enable_notifications))
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            dialogInterface.cancel();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getFragmentManagerHandler().replace(new MainTabFragment(), false);
                        }
                    })
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            loginPresenter.registerPush();
                        }
                    })
                    .show();
        else errorDialog = new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.playservices_error, getString(R.string.common_google_play_services_install_text, getString(R.string.app_name))))
                .setPositiveButton(R.string.common_google_play_services_install_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms")));
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getFragmentManagerHandler().replace(new MainTabFragment(), false);
                    }
                })
                .show();


    }

    @Override
    public void onPushRegistered() {
        getFragmentManagerHandler().replace(new MainTabFragment(), false);
    }

    @Override
    public void onLogoutSuccess() {

    }

    @Override
    public void onPushUnRegistered() {

    }

    @Override
    protected View getProgressView() {
        return progressView;
    }

    @Override
    public void onError(String message) {
        super.onError(message);
        errorDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                hideErrors();
                dialogInterface.cancel();
            }
        });
    }

    @Override
    public void showProgress() {
        super.showProgress();
        loginBtn.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        loginBtn.setEnabled(true);
    }

}
