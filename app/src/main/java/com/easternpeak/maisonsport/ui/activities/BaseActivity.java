package com.easternpeak.maisonsport.ui.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.ui.fragments.BaseFragment;
import com.easternpeak.maisonsport.utils.PreferenceUtils;

import java.util.Locale;
import java.util.prefs.Preferences;

/**
 * Created by antonandreev on 26.02.18.
 */

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseFragment.FragmentManagerHandler {

    private static final String BACKSTACK = "backstack";
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLocale(getApplicationContext());
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null)
                    actionBar.setDisplayHomeAsUpEnabled(fragmentManager.getBackStackEntryCount() > 0);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MaisonSportApplication.wrap(newBase, new Locale(PreferenceUtils.getLanguage(newBase))));
    }

    public void setLocale(Context context) {
        Locale locale = new Locale(PreferenceUtils.getLanguage(context));
        Configuration config = getResources().getConfiguration();
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M
                && Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(locale);
            config.setLocale(locale);
        } else config.setLocale(locale);
        this.getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    protected abstract int getContainerId();

    @Override
    public void add(Fragment fragment, boolean isBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .add(getContainerId(), fragment, fragment.getClass().getSimpleName());
        if (isBackStack)
            transaction.addToBackStack(BACKSTACK);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void replace(Fragment fragment, boolean isBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .replace(getContainerId(), fragment, fragment.getClass().getSimpleName());
        if (isBackStack)
            transaction.addToBackStack(BACKSTACK);
        transaction.commitAllowingStateLoss();
    }
}
