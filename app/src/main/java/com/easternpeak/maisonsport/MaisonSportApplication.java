package com.easternpeak.maisonsport;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.LocaleList;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.easternpeak.maisonsport.di.AppComponent;
import com.easternpeak.maisonsport.di.DaggerAppComponent;
import com.easternpeak.maisonsport.di.modules.ApiModule;
import com.easternpeak.maisonsport.di.modules.AppModule;
import com.easternpeak.maisonsport.di.modules.PreferenceModule;
import com.easternpeak.maisonsport.di.modules.RetrofitModule;
import com.easternpeak.maisonsport.ui.activities.MainActivity;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import io.fabric.sdk.android.Fabric;

import java.util.Locale;
import java.util.prefs.Preferences;

import javax.inject.Inject;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * Created by antonandreev on 21.02.18.
 */

public class MaisonSportApplication extends MultiDexApplication {

    private static AppComponent component;

    @Inject
    PreferenceUtils preferenceUtils;
    private static MaisonSportApplication instance;

    public static MaisonSportApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());
        instance = this;
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule())
                .retrofitModule(new RetrofitModule())
                .preferenceModule(new PreferenceModule())
                .build();
        setPicasso(this, RetrofitModule.getOkHttpBuilder(this));
    }


    public static AppComponent getComponent() {
        return component;
    }

    private void setPicasso(Context context, OkHttpClient.Builder okHttpClientBuilder) {
        Picasso.Builder picassoBuilder = new Picasso.Builder(context);
        okHttpClientBuilder
                .cache(new Cache(context.getCacheDir(), Integer.MAX_VALUE));
//        picassoBuilder.downloader(new OkHttp3Downloader(okHttpClientBuilder.build()));
        picassoBuilder.memoryCache(new LruCache(Integer.MAX_VALUE));
        picassoBuilder.defaultBitmapConfig(Bitmap.Config.RGB_565);
        try {
            Picasso.setSingletonInstance(picassoBuilder.build());
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(wrap(base, new Locale(PreferenceUtils.getLanguage(base))));
    }

    public static Context wrap(Context context, Locale newLocale) {
        Resources res = context.getResources();
        Configuration configuration = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(newLocale);
            Locale.setDefault(newLocale);
            LocaleList localeList = new LocaleList(newLocale);
            LocaleList.setDefault(localeList);
            configuration.setLocales(localeList);
            context = context.createConfigurationContext(configuration);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(newLocale);
            context = context.createConfigurationContext(configuration);
        } else {
            configuration.locale = newLocale;
            res.updateConfiguration(configuration, res.getDisplayMetrics());
        }
        return context;
    }

    public void restart() {
        Log.d("LocaleTest", "restarting...");
        Intent mStartActivity = new Intent(this, MainActivity.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int mPendingIntentId = 12345;
        PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId,
                mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        System.exit(0);
    }
}
