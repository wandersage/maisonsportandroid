package com.easternpeak.maisonsport.di.modules;


import android.content.Context;
import android.support.annotation.NonNull;

import com.easternpeak.maisonsport.BuildConfig;
import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.api.AuthInterceptor;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by anton on 11.07.17.
 */

@Module
public class RetrofitModule {
    public static final String STAGE_URL = "https://staging.maisonsport.com/";
    //    public static final String STAGE_URL = "https://requestb.in/";
    public static final String PROD_URL = "https://maisonsport.com/";

    public static final long CACHE_VALID_TIME = 60 * 60 * 24;
    private HttpUrl httpUrl;

    @Provides
    @NonNull
//    @Singleton
    public Retrofit provideRetrofit(PreferenceUtils preferenceUtils, Context context) {
        OkHttpClient.Builder builder = getOkHttpBuilder(context);
        builder
                .addNetworkInterceptor(new AuthInterceptor(preferenceUtils))
                .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC));
        String baseUrl = null;
        if ("stage".equals(BuildConfig.FLAVOR)) {
            baseUrl = STAGE_URL + "api/";
        } else if ("prod".equals(BuildConfig.FLAVOR))
            baseUrl = PROD_URL + "api/";
        baseUrl+=BuildConfig.API_VERSION_PATH;
        if (preferenceUtils.getLanguage() != null)
            baseUrl += preferenceUtils.getLanguage() + "/";

        assert baseUrl != null;
        httpUrl = HttpUrl.parse(baseUrl);
        return new Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(builder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(
                        new GsonBuilder()
                                .excludeFieldsWithoutExposeAnnotation()
                                .create()
                ))
                .build();
    }

    public static OkHttpClient.Builder getOkHttpBuilder(Context context) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(BuildConfig.DEBUG ? 10 : 20, TimeUnit.SECONDS)
                .writeTimeout(BuildConfig.DEBUG ? 10 : 20, TimeUnit.SECONDS)
                .connectTimeout(BuildConfig.DEBUG ? 5 : 20, TimeUnit.SECONDS)
//                .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .cache(null);

        return builder;
    }
}
