package com.easternpeak.maisonsport.di;

import com.easternpeak.maisonsport.api.messages.IMessagesApiClientImpl;
import com.easternpeak.maisonsport.di.modules.ApiModule;
import com.easternpeak.maisonsport.di.modules.AppModule;
import com.easternpeak.maisonsport.di.modules.PreferenceModule;
import com.easternpeak.maisonsport.di.modules.RetrofitModule;
import com.easternpeak.maisonsport.fcm.MyFirebaseInstanceIDService;
import com.easternpeak.maisonsport.mvp.presenters.BlogListPresenter;
import com.easternpeak.maisonsport.mvp.presenters.BlogPresenter;
import com.easternpeak.maisonsport.mvp.presenters.ForgotPasswordPresenter;
import com.easternpeak.maisonsport.mvp.presenters.LoginPresenter;
import com.easternpeak.maisonsport.mvp.presenters.MessagesListPresenter;
import com.easternpeak.maisonsport.mvp.presenters.NotificationsPresenter;
import com.easternpeak.maisonsport.mvp.presenters.ThreadPresenter;
import com.easternpeak.maisonsport.ui.activities.MainActivity;
import com.easternpeak.maisonsport.ui.fragments.LoginFragment;
import com.easternpeak.maisonsport.ui.fragments.SettingsFragment;

import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

import dagger.Component;


/**
 * Created by anton on 11.07.17.
 */
@Component(modules = {ApiModule.class, AppModule.class, PreferenceModule.class, RetrofitModule.class})
@Singleton
public interface AppComponent {

    void inject(LoginPresenter loginPresenter);

    void inject(@NotNull MainActivity mainTabActivity);

    void inject(MessagesListPresenter messagesListPresenter);

    void inject(IMessagesApiClientImpl iMessagesApiClient);

    void inject(SettingsFragment settingsFragment);

    void inject(ForgotPasswordPresenter forgotPasswordPresenter);

    void inject(ThreadPresenter threadPresenter);

    void inject(BlogListPresenter blogPresenter);

    void inject(BlogPresenter blogPresenter);

    void inject(LoginFragment loginFragment);

    void inject(MyFirebaseInstanceIDService myFirebaseInstanceIDService);

    void inject(NotificationsPresenter notificationsPresenter);
}