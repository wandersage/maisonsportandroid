package com.easternpeak.maisonsport.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by anton on 11.07.17.
 */

@Module
public class AppModule {

    private Context context;

    public AppModule(@NonNull Context context) {
        this.context = context;
    }
    @Provides
    @Singleton
    @NonNull
    public Context provideContext() {
        return context;
    }
}
