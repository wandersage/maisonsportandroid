package com.easternpeak.maisonsport.di.modules;

import android.support.annotation.NonNull;

import com.easternpeak.maisonsport.api.blog.IBlogApiClient;
import com.easternpeak.maisonsport.api.blog.IBlogApiClientImpl;
import com.easternpeak.maisonsport.api.login.ILoginApiClient;
import com.easternpeak.maisonsport.api.login.ILoginApiClientImpl;
import com.easternpeak.maisonsport.api.messages.IMessagesApiClient;
import com.easternpeak.maisonsport.api.messages.IMessagesApiClientImpl;
import com.easternpeak.maisonsport.api.notifications.INotificationsApiClient;
import com.easternpeak.maisonsport.api.notifications.INotificationsApiClientImpl;
import com.easternpeak.maisonsport.utils.PreferenceUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by anton on 11.07.17.
 */

@Module
public class ApiModule {

    @Provides
    @NonNull
    @Singleton
    public ILoginApiClient provideILoginApi(@NonNull Retrofit retrofit, @NonNull PreferenceUtils preferenceUtils){
        return new ILoginApiClientImpl(retrofit, preferenceUtils);
    }

    @Provides
    @NonNull
    @Singleton
    public IMessagesApiClient provideIMessagesApi(@NonNull Retrofit retrofit){
        return new IMessagesApiClientImpl(retrofit);
    }
    @Provides
    @NonNull
    @Singleton
    public IBlogApiClient provideIBlogApi(@NonNull Retrofit retrofit){
        return new IBlogApiClientImpl(retrofit);
    }

    @Provides
    @NonNull
    @Singleton
    public INotificationsApiClient provideINotificationApi(@NonNull Retrofit retrofit){
        return new INotificationsApiClientImpl(retrofit);
    }
}
