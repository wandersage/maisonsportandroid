package com.easternpeak.maisonsport.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import com.easternpeak.maisonsport.utils.PreferenceUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by anton on 11.07.17.
 */

@Module
public class PreferenceModule {

    @Provides
    @Singleton
    @NonNull
    public PreferenceUtils providePreferenceUtils(@NonNull Context context){
        return new PreferenceUtils(context);
    }

//    public AuthInterceptor provideAuthInterceptor(@NonNull PreferenceUtils preferenceUtils){
//        return new AuthInterceptor(preferenceUtils);
//    }
}
