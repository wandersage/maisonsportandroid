/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.easternpeak.maisonsport.fcm;

import android.util.Log;

import com.easternpeak.maisonsport.MaisonSportApplication;
import com.easternpeak.maisonsport.api.login.ILoginApiClient;
import com.easternpeak.maisonsport.utils.PreferenceUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    public static final String TAG = "MyFirebaseIIDService";


    @Inject
    ILoginApiClient apiClient;
    @Inject
    PreferenceUtils preferenceUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        MaisonSportApplication.getComponent().inject(this);
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        if (refreshedToken != null) {
            sendToken(refreshedToken);
        }
    }

    private void sendToken(final String refreshedToken) {
        apiClient.registerPush(refreshedToken).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                Log.d(TAG, "register token: " + aBoolean);
                if (aBoolean)
                    preferenceUtils.setPushToken(refreshedToken);
//                else {
//                    preferenceUtils.setPushToken(null);
//                    sendToken(refreshedToken);
//                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Log.d(TAG, "register token: failed", throwable);
            }
        });
    }

}
