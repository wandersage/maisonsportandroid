/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.easternpeak.maisonsport.fcm;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.easternpeak.maisonsport.R;
import com.easternpeak.maisonsport.ui.activities.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static final String ACTION_MESSAGE_RECEIVED = "com.easternpeak.maisonsport.fcm.ACTION_MESSAGE_RECEIVED";
    public static final String EXTRA_NOTIFICATION_ID = "com.easternpeak.maisonsport.fcm.EXTRA_NOTIFICATION_ID";

    public static int ID = 0;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet())
            Log.d(TAG, entry.getKey() + " " + entry.getValue());
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "notification " + remoteMessage.getNotification().getBody() + " " + remoteMessage.getNotification().getTitle());
            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle(), remoteMessage.getData());
        } else if (remoteMessage.getData().size() > 0) {
            String message = remoteMessage.getData().get("cust_message");
            String title = remoteMessage.getData().get("cust_title");
            Log.d(TAG, "payload " + message + " " + title);
            sendNotification(message, title, remoteMessage.getData());
        }

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String title, @Nullable Map<String, String> entry) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("extra_message", messageBody);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (entry != null)
            for (Map.Entry<String, String> entry1 : entry.entrySet())
                intent.putExtra(entry1.getKey(), entry1.getValue());

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, TAG)
                .setChannelId(TAG)
                .setSmallIcon(R.drawable.ic_bring)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentText(messageBody)
                .setContentTitle(title != null ? title : getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(TAG, TAG, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(mChannel);
            }
            final int id = ID++;
            notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
            sendUpdateIntent(id,entry);
        } else Log.d(TAG, "notificationManager==null");
    }

    private void sendUpdateIntent(final int notificationId, Map<String, String> entry) {
        Intent intent = new Intent(ACTION_MESSAGE_RECEIVED);
        for (Map.Entry<String, String> entry1 : entry.entrySet())
            intent.putExtra(entry1.getKey(), entry1.getValue());
        intent.putExtra(EXTRA_NOTIFICATION_ID,notificationId);
        sendBroadcast(intent);
    }
}
